using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CajonSimple : MonoBehaviour
{
    public  float cuandoAbre ;
    public float velocidad;
    

    [Header("Informativo")]
    public float difCajonAbierto;

    bool abierto = false;
    bool abriendo = false;
    bool cerrando = false;
    Vector3 posAbierto;

    Vector3 posCerrado;

 
    // Start is called before the first frame update
    void Start()
    {
      posCerrado = transform.localPosition;
      posAbierto = new Vector3(transform.localPosition.x - cuandoAbre, transform.localPosition.y, transform.localPosition.z);
    }

    // Update is called once per frame
    void Update()
    {

       // difCajonAbierto = posCerrado.x - transform.localPosition.x;
       
if (abriendo)
{
  transform.localPosition = Vector3.MoveTowards(transform.localPosition,posAbierto, Time.deltaTime * velocidad);
if (Vector3.Distance(transform.localPosition, posAbierto) < 0.0001F)
{
  abriendo=false;
  abierto = true;
}
}
if (cerrando)
{
  transform.localPosition = Vector3.MoveTowards(transform.localPosition,posCerrado, Time.deltaTime * velocidad);
if (Vector3.Distance(transform.localPosition, posCerrado) < 0.0001F)
{
  cerrando=false;
  abierto = false;
}
}
//if(Input.GetButtonDown("Fire2")) AbreCierra();
    }
   
   public void AbreCierra() {
     if (!abierto) abriendo = true;
      else cerrando = true;
   }
    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
           abriendo = true;
           cerrando= false;
        }
    }
        void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            abriendo = false;
             cerrando= true;
        }
    }
}
